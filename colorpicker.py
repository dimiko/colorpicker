# -*- coding: utf-8 -*-

#Uses ColorThief: https://github.com/fengsp/color-thief-py
#pip install colorthief

#Reads image file from -i argument. Prints images dominant color and palette of colors from image.
#if quality, colorcount or palettequality is not defined from commandline arguments their values are set 
# to default values (quality = 1, palettequality = 10 and colorcount = 6).
#Outputs palette colors to new image file if outputpalette(-o) argument is given.
#Detects color images from given path -i if detectcolorimages(-d) argument is given.
#sorts color images to seperate folders by dominant color when using --detectcolorimages with sortbydominant(-s)

#param quality(-q): quality settings, 1 is the highest quality, the bigger
#                        the number, the faster a color will be returned but
#                        the greater the likelihood that it will not be the
#                        visually most dominant color

#param colorcount(-c): the size of the palette, max number of colors

#param palettequality(-p): quality settings, 1 is the highest quality, the bigger
#                        the number, the faster the palette generation, but the
#                        greater the likelihood that colors will be missed.

#param outputpalette(-o): writes palette colors to new image file. Image files name is input filename + _palette.png

#param detectcolorimages(-d): detects color images from given path -i

#param sortbydominant(-s): sorts color images to seperate folders by dominant color when using --detectcolorimages

#Examples:
#Prints 'test2.jpg' images dominant color and palette of colors from image.
#python colorpicker.py -i test2.jpg

#detects color images from given path 'test/' and sorts color images by dominant color and copies them to 'dominant_results/' folder's subfolders
#python colorpicker.py -i test/ -d -s

from colorthief import ColorThief
from optparse import OptionParser
from shutil import copyfile
import cv2
import numpy as np
import os
import webcolors

def detectColorImages(path, colorcount, quality, onlyDominant):

    colorImages = []
    fileCounter = 0
    totalFilesCount = 0
    currentSubdir = 0

    for subdir, dirs, files in os.walk(path):
        for file in files:

            fileCounter+=1

            if currentSubdir is not subdir:
                currentSubdir = subdir
                totalFilesCount += len(files)

            print 'file: ' + str(fileCounter) + '/' + str(totalFilesCount)
            print 'current subdir: ' + subdir

            if not 'tmp' in subdir and not 'output' in subdir:
                imagePath = os.path.join(subdir, file)

                print imagePath

                try:
                    color_thief = ColorThief(imagePath)

                    if (onlyDominant):
                        dominant_color = color_thief.get_color(quality)
                        if not dominant_color[0] == dominant_color[1] == dominant_color[2]:
                            colorImages.append(imagePath)
                            color_name = get_colour_name(dominant_color)#webcolors.rgb_to_name(dominant_color)
                            directory = 'dominant_results/' + color_name + '/'
                            if not os.path.exists(directory):
                                os.makedirs(directory)

                            copyfile(imagePath, directory + file)
                            print 'color image: '  + imagePath
                            with open("colorimages2.txt", "a") as log:
                                log.write(imagePath + '-->'  + directory + file +'\n')
                    else:
                        palette = color_thief.get_palette(colorcount, quality)

                        for color in palette:
                            if not color[0] == color[1] == color[2]:
                                colorImages.append(imagePath)
                                copyfile(imagePath, 'results/' + file)
                                print 'color image: '  + imagePath
                                with open("colorimages.txt", "a") as log:
                                    log.write(imagePath + '-->'  + 'results/' + file +'\n')

                                break
                except IOError:
                    print 'Not valid image: ' + imagePath
                    with open("notvalidimage.txt", "a") as errorlog:
                        errorlog.write('Not valid image: '  + imagePath +'\n')
            
def drawPaletteImage(palette, inputfile):
    # Create a black image
    img = np.zeros((50,len(palette) * 50,3), np.uint8)

    colorCounter = 0

    for color in palette:
        #print color
        cv2.rectangle(img,(colorCounter * 50,0),(50 + colorCounter * 50,50),color,-1)
        #cv2.imshow('image',img)
        #cv2.waitKey(0)
        #cv2.imwrite('sample_out_1.png', img)
        colorCounter +=1
    
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    #removes .jpg from inputfile and adds _palette.jpg
    outputPalette = inputfile.split('.', 1)[0] + '_palette.png'

    cv2.imwrite(outputPalette, img)

    print 'Palette image created: ' + outputPalette


def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    
    if actual_name is not None:
        return actual_name
    else:
        return closest_name

parser = OptionParser()
parser.add_option("-i", "--input", type="string", dest="input", help="Input file name", metavar="input")
parser.add_option("-q", "--quality", type="int", dest="quality", help="quality", metavar="quality")
parser.add_option("-p", "--palettequality", type="int", dest="palettequality", help="palette quality", metavar="palettequality")
parser.add_option("-c", "--colorcount", type="int", dest="colorcount", help="palette color count", metavar="colorcount")
parser.add_option("-o", "--outputpalette", action="store_true", dest="outputpalette", help="output palette image", metavar="outputpalette")
parser.add_option("-d", "--detectcolorimages", action="store_true", dest="detectcolorimages", help="detects color images from given path -i", metavar="detectcolorimages")
parser.add_option("-s", "--sortbydominant", action="store_true", dest="sortbydominant", help="sorts color images to seperate folders by dominant color when using --detectcolorimages", metavar="sortbydominant")

(options, args) = parser.parse_args()
inputfile = options.input
quality = options.quality
palettequality = options.palettequality
colorcount = options.colorcount
outputpalette = options.outputpalette
detectcolorimages = options.detectcolorimages
sortbydominant = options.sortbydominant


if (not quality):
    quality = 1

if (not palettequality):
    palettequality = 10

if (not colorcount or colorcount < 2):
    colorcount = 6

if (inputfile and not detectcolorimages):

    color_thief = ColorThief(inputfile)
    # get the dominant color
    dominant_color = color_thief.get_color(quality=quality)
    # build a color palette
    palette = color_thief.get_palette(color_count=colorcount, quality=palettequality)

    print 'dominant_color: ' + str(dominant_color)
    print 'palette: ' + str(palette)

    if (outputpalette):
        drawPaletteImage(palette, inputfile)

elif(inputfile and detectcolorimages):
    if sortbydominant:
        detectColorImages(inputfile, colorcount, quality, sortbydominant)
    else:
        detectColorImages(inputfile, colorcount, palettequality, sortbydominant)

else:
    print 'Set input file from commandline argument -i: colorpicker.py -i inputfile'